Farmdrop Test
=============

There is an API with routes to get Producers, Line Items and Transfers. These
are backed by an SQL database.

There are also a command line with two options, one to queue up transfers, and
one to perform transfers. In production these could be set up to run via a cron
job. Or, if the application was also in control of line item creations, I could
create a hook that ran them immediately.

TODO
----

If I was to continue working on it, I would have addressed the following:

- No rake task to run all the tests (I've been manually running the tests on
  each file).
- No config.ru file, so there is no way to actually run the API at the moment,
  other than via the tests.
- There is no way to generate some dummy data in order to use the API.
- I have not got to the point of using the actual stripe API, I've left in a
  couple of TODO comments where I would have added the calls.
- There is no API route to get a list of all producers, so at the moment you
  need to know the id of the producer to get that API route.
- I could probably also have created some API routes that list all of the
  queued transfers, and all of the line items with no transfer associated with
  them.
- I was going to create a simple Sinatra app that consumed the API in order to
  display the information in a html frontend. At the moment, the data is all in
  JSON format (or a html representation of the JSON data, which comes for free
  with the Doze framework). This could also have been done with a single page
  application, or from a Rails app. My preference is to keep the API and the
  client separate, even if the client is calling the API on the server side, as
  I think it nicely separates the logic, and makes changing one of them easier.
