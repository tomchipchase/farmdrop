Sequel.migration do
  change do
    create_table(:producers) do
      primary_key :id
      String :name, null: false
      String :stripe_id, default: nil
      BigDecimal :farmdrop_rate, default: 0.25
    end

    create_table(:line_items) do
      primary_key :id
      String :name
      BigDecimal :amount
      foreign_key :producer_id, :producers
    end
  end
end
