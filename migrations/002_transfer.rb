Sequel.migration do
  change do
    create_table(:transfers) do
      primary_key :id
      BigDecimal :amount
      String :recipient_stripe_id
      String :status
      foreign_key :line_item_id, :line_items
    end
  end
end
