require 'farmdrop'

require 'farmdrop/actions/line_item_finder'
require 'farmdrop/actions/stripe_recipient_creator'
require 'farmdrop/actions/transfer_performer'
require 'farmdrop/actions/transfer_queuer'

module Farmdrop
  module Actions
  end

  def self.actions
    @actions ||=
      begin
        struct = Struct.new(:line_item_finder, :stripe_recipient_creator,
                            :transfer_queuer, :transfer_performer)

        config = Farmdrop.config
        repos = Farmdrop.repositories

        stripe_recipient_creator = Action::StripeRecipientCreator.new(
          config, repos.producer)

        transfer_queuer = Action::TransferQueuer.new(repos.transfer,
                                                     stripe_recipient_creator)

        transfer_performer = Action::TransferPerformer.new(repos.transfer)
        line_item_finder = Action::LineItemFinder.new(repos.line_item)

        struct.new(line_item_finder, stripe_recipient_creator, transfer_queuer,
                   transfer_performer)
      end
  end
end
