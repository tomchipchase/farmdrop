require 'farmdrop/actions'

module Farmdrop
  module CLI
    class ProcessTransfers < Climate::Command('process_transfers')
      subcommand_of Root

      description 'Process any pending transfers'

      def run
        Farmdrop.actions.transfer_performer.call
      end
    end
  end
end
