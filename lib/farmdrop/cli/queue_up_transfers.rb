require 'farmdrop/actions'

module Farmdrop
  module CLI
    class QueueUpTransfers < Climate::Command('queue_transfers')
      subcommand_of Root

      description "Queue up transfers for all line items that haven't been paid"

      def run
        line_items = Farmdrop.actions.line_item_finder.call
        line_items.each do |line_item|
          Farmdrop.actions.transfer_queuer.call(line_item)
        end
      end
    end
  end
end
