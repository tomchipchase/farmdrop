module Farmdrop
  module Action
    # Class to create a Stripe recipient, if one does not already exist.
    class StripeRecipientCreator
      def initialize(config, producer_repository)
        @stripe_api_key = config.stripe_api_key
        @producer_repository = producer_repository
      end

      def call(producer)
        if producer.stripe_id
          # Account already exists, do nothing
          # TODO: check account is valid
          return false
        end

        # TODO: create account
        producer.stripe_id = rand(1_000_000)
        @producer_repository.update(producer)

        true
      end
    end
  end
end
