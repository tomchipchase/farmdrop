module Farmdrop
  module Action
    # Finds the line items that do not have an associated transfer.
    class LineItemFinder
      def initialize(line_item_repo)
        @line_item_repo = line_item_repo
      end

      def call
        @line_item_repo.get_all.select { |li| li.transfers.count == 0 }
      end
    end
  end
end
