module Farmdrop
  module Action
    # Queues up a transfer to a producer.
    class TransferQueuer
      def initialize(transfer_repo, stripe_recipient_creator)
        @transfer_repo = transfer_repo
        @stripe_recipient_creator = stripe_recipient_creator
      end

      def call(line_item)
        @line_item = line_item

        transfer = Model::Transfer.new(recipient_stripe_id: recipient_stripe_id,
                                       amount: amount,
                                       status: 'pending',
                                       line_item: line_item)

        @transfer_repo.store_new(transfer)
      end

      private

      def amount
        farmdrop_rate = @line_item.producer.farmdrop_rate
        amount = @line_item.amount
        amount * (1 - farmdrop_rate)
      end

      def recipient_stripe_id
        unless @line_item.producer.stripe_id
          @stripe_recipient_creator.call(@line_item.producer)
        end
      end
    end
  end
end
