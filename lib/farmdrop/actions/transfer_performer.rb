module Farmdrop
  module Action
    # Perform a queued transfer
    class TransferPerformer
      def initialize(transfer_repo)
        @transfer_repo = transfer_repo
      end

      def call
        transfers = @transfer.repo.get_by_property(:status, 'pending')

        transfers.each do |transfer|
          # TODO: call to stripe

          transfer.status = :done
          @transfer_repo.update(transfer)
        end
      end
    end
  end
end
