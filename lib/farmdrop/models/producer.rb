require 'thin_models/struct/identity'

module Farmdrop
  module Model
    # Model for representing a producer
    class Producer < ThinModels::Struct
      identity_attribute
      attribute :name
      attribute :stripe_id
      attribute :stripe_type
      attribute :farmdrop_rate
      attribute :line_items
    end
  end
end
