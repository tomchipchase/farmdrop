require 'thin_models/struct/identity'

module Farmdrop
  module Model
    # Model for representing a transfer
    class Transfer < ThinModels::Struct
      identity_attribute
      attribute :amount
      attribute :recipient_stripe_id
      attribute :line_item
      attribute :status
    end
  end
end
