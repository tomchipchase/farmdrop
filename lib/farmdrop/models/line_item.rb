require 'thin_models/struct/identity'

module Farmdrop
  module Model
    # Model for representing a line item
    class LineItem < ThinModels::Struct
      identity_attribute
      attribute :name
      attribute :amount
      attribute :producer
      attribute :transfers
    end
  end
end
