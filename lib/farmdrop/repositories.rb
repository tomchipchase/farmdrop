require 'farmdrop'
require 'farmdrop/repositories/producer'
require 'farmdrop/repositories/line_item'
require 'farmdrop/repositories/transfer'

module Farmdrop
  def self.repositories
    @repositories ||=
      begin
        struct = Struct.new(:producer, :line_item, :transfer)
        producer = Repo::Producer.new(db)
        line_item = Repo::LineItem.new(db)
        transfer = Repo::Transfer.new(db)

        producer.mapper(:line_items).target_repo = line_item

        line_item.mapper(:producer).target_repo = producer
        line_item.mapper(:transfers).target_repo = transfer

        transfer.mapper(:line_item).target_repo = line_item

        struct.new(producer, line_item, transfer)
      end
  end
end
