require 'hold/sequel'

require 'farmdrop/models/producer'
require 'farmdrop/models/line_item'

module Farmdrop
  module Repo
    # Repository for persisting producer objects
    class Producer < Hold::Sequel::IdentitySetRepository
      set_model_class Model::Producer
      use_table :producers, id_sequence: true

      map_column :name
      map_column :stripe_id
      map_column :farmdrop_rate

      map_one_to_many :line_items, model_class: Model::LineItem,
                                   property: :producer,
                                   writeable: true
    end
  end
end
