require 'hold/sequel'

require 'farmdrop/models/producer'
require 'farmdrop/models/line_item'
require 'farmdrop/models/transfer'

module Farmdrop
  module Repo
    # Repository for persisting line items
    class LineItem < Hold::Sequel::IdentitySetRepository
      set_model_class Model::LineItem
      use_table :line_items, id_sequence: true

      map_column :name
      map_column :amount

      map_foreign_key :producer, model_class: Model::Producer
      map_one_to_many :transfers, model_class: Model::Transfer,
                                  property: :line_item
    end
  end
end

