require 'hold/sequel'

require 'farmdrop/models/transfer'
require 'farmdrop/models/line_item'

module Farmdrop
  module Repo
    class Transfer < Hold::Sequel::IdentitySetRepository
      set_model_class Model::Transfer
      use_table :transfers, id_sequence: true

      map_column :amount
      map_column :recipient_stripe_id
      map_column :status

      map_foreign_key :line_item, model_class: Model::LineItem
    end
  end
end
