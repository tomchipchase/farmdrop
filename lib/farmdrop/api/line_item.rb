require 'doze'

module Farmdrop
  module API
    class LineItem
      include Doze::Resource
      include Doze::Serialization::Resource

      def initialize(uri, id, repo)
        @uri = uri
        @id = id
        @repo = repo
      end

      def exists?
        @line_item = @repo.get_by_id(@id)
      end

      def get_data
        {
          href: @uri,
          data: {
            id: @line_item.id,
            amount: @line_item.amount,
            producer: { id:  @line_item.producer.id },
            transfers: @line_item.transfers.map { |t| { id: t.id } }
          }
        }
      end
    end
  end
end
