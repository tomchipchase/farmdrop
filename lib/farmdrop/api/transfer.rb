require 'doze'

module Farmdrop
  module API
    class Transfer
      include Doze::Resource
      include Doze::Serialization::Resource

      def initialize(uri, id, repo)
        @uri = uri
        @id = id
        @repo = repo
      end

      def exists?
        @transfer = @repo.get_by_id(@id)
      end

      def get_data
        {
          href: @uri,
          data: {
            id: @transfer.id,
            amount: @transfer.amount,
            line_item: { id: @transfer.line_item.id },
            producer: { id:  @transfer.line_item.producer.id }
          }
        }
      end
    end
  end
end
