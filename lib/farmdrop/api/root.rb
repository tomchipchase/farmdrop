require 'doze'

require 'farmdrop/api/producer'
require 'farmdrop/api/line_item'
require 'farmdrop/api/transfer'

module Farmdrop
  module API
    # Root of the API
    class Root
      include Doze::Router
      include Doze::Resource
      include Doze::Serialization::Resource

      def initialize(repositories)
        @repos = repositories
      end

      route '/producer/{id}' do |router, uri, params|
        router.producer_resource(uri, params)
      end

      def producer_resource(uri, params)
        Producer.new(uri, params[:id], @repos.producer)
      end

      route '/line_item/{id}' do |router, uri, params|
        router.line_item(uri, params)
      end

      def line_item(uri, params)
        LineItem.new(uri, params[:id], @repos.line_item)
      end

      route '/transfer/{id}' do |router, uri, params|
        router.transfer(uri, params)
      end

      def transfer(uri, params)
        Transfer.new(uri, params[:id], @repos.transfer)
      end
    end
  end
end
