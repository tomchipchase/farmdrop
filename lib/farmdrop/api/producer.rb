require 'doze'

module Farmdrop
  module API
    # Producer API resource
    class Producer
      include Doze::Resource
      include Doze::Serialization::Resource

      def initialize(uri, id, repo)
        @uri = uri
        @id = id
        @repo = repo
      end

      def exists?
        @producer = @repo.get_by_id(@id)
      end

      def get_data
        {
          href: @uri,
          data: {
            id: @producer.id,
            name: @producer.name,
            line_items: @producer.line_items.map { |li| { id: li.id } },
            transfers: @producer.line_items.map { |li| li.transfers }.flatten
                                .map { |t| { id: t.id } }
          }
        }
      end
    end
  end
end
