require 'climate'

module Farmdrop
  module CLI
    class Root < Climate::Command('farmdrop')
    end

    require 'farmdrop/cli/queue_up_transfers'
    require 'farmdrop/cli/process_transfers'

    def self.main
      Climate.with_standard_exception_handling do
        Root.run(ARGV)
      end
    end
  end
end

