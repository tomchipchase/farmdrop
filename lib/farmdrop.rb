require 'logger'

require 'farmdrop/api/root'
require 'farmdrop/repositories'

# Top level namespace
module Farmdrop
  def self.api
    @api ||= begin
               root = API::Root.new(repositories)
               Doze::Application.new(root)
             end
  end

  def self.db
    @db ||= Sequel.connect(config.database_url)
  end

  def self.config
    Struct.new(:database_url, :stripe_api_key).new(
      ENV.fetch('DATABASE_URL', 'sqlite://farmdrop.db'),
      ENV.fetch('STRIPE_API_KEY', 'sk_test_hccH7eUZcLPPyXWt3DRxEWDH'))
  end
end
