require_relative 'test_helper'

class TestLineItem < MiniTest::Test
  include Rack::Test::Methods

  def setup
    @repo = Farmdrop.repositories.line_item

    @producer_repo = Farmdrop.repositories.producer
    @producer = Farmdrop::Model::Producer.new(name: 'foo')
    @producer_repo.store(@producer)

    line_item_repo = Farmdrop.repositories.line_item
    @line_item = Farmdrop::Model::LineItem.new(producer: @producer)
    line_item_repo.store(@line_item)

    transfer_repo = Farmdrop.repositories.transfer
    @transfer = Farmdrop::Model::Transfer.new(line_item: @line_item)
    transfer_repo.store(@transfer)
  end

  def test_can_get_a_line_item
    get "/line_item/#{@line_item.id}"
    assert last_response.ok?
    json = JSON.parse(last_response.body)['data']
    assert_equal @line_item.id, json['id']
  end

  def test_can_get_a_producer
    get "/producer/#{@producer.id}"
    assert last_response.ok?
    json = JSON.parse(last_response.body)['data']
    assert_equal @producer.name, json['name']
  end

  def test_includes_line_items
    line_item = Farmdrop::Model::LineItem.new
    @producer.line_items = [line_item]
    @producer_repo.store(@producer)

    get "/producer/#{@producer.id}"
    assert last_response.ok?
    json = JSON.parse(last_response.body)['data']
    assert_equal @producer.line_items.count, json['line_items'].count
  end

  def test_can_get_a_transfer
    get "/transfer/#{@transfer.id}"
    assert last_response.ok?
    json = JSON.parse(last_response.body)['data']
    assert_equal @transfer.id, json['id']
  end
end

