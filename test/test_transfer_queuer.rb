require_relative 'test_helper'

require 'farmdrop/actions'

class TestTransferQueuer < MiniTest::Test
  def setup
    id = Farmdrop.repositories.producer.store(
      Farmdrop::Model::Producer.new(name: 'foo')).id

    @producer = Farmdrop.repositories.producer.get_by_id(id)

    @line_item = Farmdrop::Model::LineItem.new(amount: 10, producer: @producer)
    Farmdrop.repositories.line_item.store(@line_item)
  end

  def test_creates_stripe_recipient
    Farmdrop.actions.transfer_queuer.call(@line_item)
    Farmdrop.repositories.producer.reload(@producer)
    refute_nil @producer.stripe_id
  end

  def test_transfer_created
    Farmdrop.actions.transfer_queuer.call(@line_item)
    refute_nil Farmdrop.repositories.transfer.get_by_property :status, 'pending'
  end
end
