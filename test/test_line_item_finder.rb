require_relative 'test_helper'

require 'farmdrop/actions/transfer_queuer'
require 'farmdrop/actions/line_item_finder'

class TestLineItemFinder < MiniTest::Test
  def setup
    producer_repo = Farmdrop.repositories.producer
    producer = Farmdrop::Model::Producer.new(name: 'foo')
    producer_repo.store_new(producer)

    line_item_repo = Farmdrop.repositories.line_item

    10.times do
      line_item = Farmdrop::Model::LineItem.new(producer: producer, amount: 10)
      line_item_repo.store(line_item)
    end

    @line_item_finder =
      Farmdrop::Action::LineItemFinder.new(Farmdrop.repositories.line_item)
  end

  def test_queues_up_transfers
    line_items = @line_item_finder.call
    assert_equal 10, line_items.count
  end
end
