ROOT = File.join(File.dirname(__FILE__), '..')

$LOAD_PATH << File.join(ROOT, 'lib')

require 'minitest/autorun'
require 'rack/test'
require 'farmdrop'
require 'sequel'

ENV['RACK_ENV'] = 'test'
ENV['DATABASE_URL'] = 'sqlite:/'

def app
  Farmdrop.api
end

Sequel.extension :migration
Sequel::Migrator.run(Farmdrop.db, File.join(ROOT, 'migrations'))
